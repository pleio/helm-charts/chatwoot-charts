# chatwoot

![Version: 0.1.11](https://img.shields.io/badge/Version-0.1.11-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

## Source Code

* <https://gitlab.com/pleio/helm-charts/chatwoot-charts>

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | redis | 20.11.2 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| enableMailstub | bool | `true` |  |
| enableWeb | bool | `true` |  |
| hostname | string | `"chart-example.local"` |  |
| imageRepository | string | `"registry.gitlab.com/pleio/chatwoot"` | Default image repository for the Chatwoot application. |
| imageTag | string | `""` | Default Chatwoot application image tag to use. |
| mailstubHostname | string | `"mailstub.chart-example.local"` |  |
| mailstubImageRepository | string | `"maildev/maildev"` |  |
| mailstubImageTag | string | `""` |  |
| mediaSharedStorageSize | string | `"1Gi"` |  |
| redis.architecture | string | `"standalone"` |  |
| redis.auth.password | string | `""` |  |
| redis.enabled | bool | `true` |  |
| redis.master.persistence.enabled | bool | `true` |  |
| redis.master.persistence.storageClass | string | `""` |  |
| redis.master.resources.limits.cpu | string | `"500m"` |  |
| redis.master.resources.limits.memory | string | `"512Mi"` |  |
| redis.master.resources.requests.cpu | string | `"250m"` |  |
| redis.master.resources.requests.memory | string | `"256Mi"` |  |
| redis.volumePermissions.enabled | bool | `true` |  |
| sharedStorageClassName | string | `""` |  |
| sidekigReplicaCount | int | `1` | Amount of replicas used for the Rails sidekiq deployment. |
| sidekiq.extraEnv | list | `[]` |  |
| sidekiqResources | object | `{}` |  |
| smtpDisableTlsUpgrade | bool | `true` |  |
| smtpTlsOnly | bool | `false` |  |
| useLetsEncryptCertificate | bool | `true` |  |
| web.extraEnv | list | `[]` |  |
| web.service.annotations | object | `{}` |  |
| webReplicaCount | int | `1` | Amount of replicas used for the Rails web deployment. |
| webResources.limits.cpu | string | `"500m"` |  |
| webResources.limits.memory | string | `"512Mi"` |  |
| webResources.requests.cpu | string | `"250m"` |  |
| webResources.requests.memory | string | `"256Mi"` |  |
| webWhitelist | string | `"chart-example.local"` |  |
| wildcardTlsSecretName | string | `""` |  |
