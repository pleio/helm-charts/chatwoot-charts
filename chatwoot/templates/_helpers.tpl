{{/*
The name of the release, used as label or a base for other names.
*/}}
{{- define "chatwoot.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end }}

{{/*
The name used to label resources associated with the web deployment.
*/}}
{{- define "chatwoot.webName" -}}
{{- printf "%s-web" (include "chatwoot.name" . )  -}}
{{- end }}

{{/*
The name used to label resources associated with the Sidekiq deployment.
*/}}
{{- define "chatwoot.sidekiqName" -}}
{{- printf "%s-sidekiq" (include "chatwoot.name" . )  -}}
{{- end }}

{{/*
The name used to label resources associated with the mailstub deployment.
*/}}
{{- define "chatwoot.mailstubName" -}}
{{- printf "%s-mailstub" (include "chatwoot.name" . )  -}}
{{- end }}

{{/*
The name of the Persistent Volume Claim containing media files, i.e. images and documents.
*/}}
{{- define "chatwoot.mediaSharedStoragePvcName" -}}
{{- printf "%s-media" (include "chatwoot.name" . ) -}}
{{- end }}

{{/*
A set of standard labels as defined by the Helm developers to use in a chart.
*/}}
{{- define "chatwoot.standardLabels" -}}
app.kubernetes.io/name: {{ include "chatwoot.name" . }}
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
app.kubernetes.io/managed-by: {{ .Release.Service }}	
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
A set of selector labels used throughout the Chart.
*/}}
{{- define "chatwoot.selectorLabels" -}}
app.kubernetes.io/name: {{ include "chatwoot.name" . }}
app.kubernetes.io/managed-by: {{ .Release.Service }}	
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}
